# Redux Integration

Well, you can see that we have `counter.js` file component inside `components` directory. This component is called inside `pages/index.js`.

But, so far, this `Counter` component is just a only a component with a view, without a functionality.

So actually, we also already have a simple `redux` store inside the `store` directory. We can use the store, integrating it with our Next.js application, and using it to supply data and functionality to our Counter components.

The question is, how do we integrate our Next.js app with our redux store?
