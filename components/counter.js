const Counter = () => (
  <div id="counter">
    <h1>0</h1>
    <div className="action-btns">
      <button>+</button>
      <button>-</button>
    </div>

    <style jsx>{`
      #counter {
        width: 100%;
      }
      h1 {
        text-align: center
      }
      .action-btns {
        display: flex;
        align-items: center;
        justify-content: center;

        button {
          padding: 8px;
          margin: 8px;
        }
      }
    `}</style>
  </div>
)

export default Counter