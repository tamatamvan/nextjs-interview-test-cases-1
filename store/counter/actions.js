import { INC, DEC } from './actionType'

export const inc = () => ({
  type: INC
})

export const dec = () => ({
  type: DEC
})
